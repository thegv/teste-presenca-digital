import React, { Component } from 'react'

class ChannelGridHeader extends Component {

    render() {
        const { columns } = this.props                
        return (
            <div className="columns cg-header is-mobile">
                <div className="column is-1">&nbsp;</div> 
                {columns.map(column =>
                    <div key={column} className="column">
                        Canal {column}
                    </div>
                )}
            </div>
        )
    }

}

export default ChannelGridHeader;