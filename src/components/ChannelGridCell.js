import React, { Component } from 'react'

class ChannelGridCell extends Component {
    render() {
        const { column } = this.props            

        return (
            <div className="column" data-channel={column}>
                
            </div>
        )
    }
}

export default ChannelGridCell