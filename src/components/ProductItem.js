import React, { Component } from 'react'
import ChannelComponent from './ChannelComponent'
import { toCurrency } from '../utils'

class ProductItem extends Component {

    render() {
        const { product } = this.props

        return (
            <div className="column is-one-third-tablet is-one-quarter-desktop">
                <div className="product">
                    <header>
                        <div className="product__type"><img src={'/img/' + product.tipo + '.svg'} width="30" alt="" /></div>
                        <h2 className="product__title">
                            {product.nome}
                        </h2>
                    </header>
                    <p className="product__description">{product.descricao}</p>

                    <ul className="columns channel-list is-mobile">
                        {
                            product.listaCanais ? (

                                product.listaCanais.map(c =>
                                    <ChannelComponent key={c.id} channel={c} />
                                )
                            ) : ''
                        }
                    </ul>


                    <p className="product__price--from">De: {'R$ ' + toCurrency(product.precoDe)}</p>
                    <p className="product__price">Por: {product.preco > 0 ? 'R$ ' + toCurrency(product.preco) : 'Grátis'}</p>
                    <p className="product__membership">Adesão: {product.adesao > 0 ? 'R$ ' + toCurrency(product.adesao) : 'Grátis'}</p>
                    <p className="product__tax">Instalação: {product.taxaInstalacao > 0 ? 'R$ ' + toCurrency(product.taxaInstalacao) : 'Grátis'}</p>

                    <button className="product__button">{product.tipo} {product.id}</button>
                </div>
            </div>
        );
    }

};

export default ProductItem