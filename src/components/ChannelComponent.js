import React from 'react'

const ChannelComponent = ({ channel }) => {

    return (
        <li className="column">
            <img alt={channel.nome} title={channel.descricao} src={channel.imagem} />
        </li>
    )
}

export default ChannelComponent