import React, { Component } from 'react'
import ChannelGridCell from './ChannelGridCell'

class ChannelGridRow extends Component {
    render() {
        const { columns, time } = this.props            

        return (
            <div className="columns is-mobile cg-row is-gapleless" data-time={time}>
                <div className="column cg-time is-1">
                    <h5>{time}</h5>
                </div>
                {columns.map((column, i) => (
                    <ChannelGridCell key={i} column={column} />
                ))}
            </div>
        )
    }
}

export default ChannelGridRow