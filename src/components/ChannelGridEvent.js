import React, { Component } from 'react'

class ChannelGridEvent extends Component {
    render() {
        const { event } = this.props

        return (
            <div className="cg-event__container">
                <div className="cg-event">
                    <h4>event.titulo</h4>
                </div>
            </div>
        )
    }
}

export default ChannelGridEvent

