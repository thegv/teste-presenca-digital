import { GET_PRODUCTS, GET_CHANNELS_GRID } from '../constants/index'
import api from '../api/api'

/**
 * PRODUCTS 
 */
const loadProducts = (products) => {
    return {
        type: GET_PRODUCTS,
        products: products
    }
}

export const setVisibilityFilter = (filter) => {
    return {
        type: 'SET_VISIBILITY_FILTER',
        filter
    }
}


/**
 * CHANNEL GRID
 */
const loadChannelGrid = (channels) => {
    return {
        type: GET_CHANNELS_GRID,
        channels
    }
}

/**
 * Action Creators
 */
export function getProducts() {
    return dispatch => {
        api.getProducts(products => {
            dispatch(loadProducts(products))
        })
    }
}

export function getChannels() {
    return dispatch => {
        api.getChannels(channels => {
            dispatch(loadChannelGrid(channels))
        })
    }
}
