import React, { Component, PropTypes } from 'react'
// -------------------------------------------
//  components
// -------------------------------------------
import ProductList from './ProductList'
import ChannelGrid from './ChannelGrid'
// -------------------------------------------
//  styles
// -------------------------------------------
import '../styles/App.css';

class App extends Component {

    showComponent(component) {
        switch (component) {
            case 'channel-grid':
                return <ChannelGrid />                
            case 'product-list':
                return <ProductList />
            default:
                return false
        }
    }

    render() {
        const component = this.props.component

        return (
            <div className="container-fluid">
                <h1 className="page__title">Teste Hiits</h1>
                
                {this.showComponent(component)}
            </div>
        );
    }
};

App.contextTypes = {
    store: PropTypes.object
}

export default App