import React, { Component } from 'react'
import { connect } from 'react-redux'

import ChannelGridHeader from '../components/ChannelGridHeader'
import ChannelGridRow from '../components/ChannelGridRow'

import moment from 'moment'
import $ from 'jquery'


class ChannelGrid extends Component {

    renderEvents(events) {
        events.map(evt => (
            this.createEvent(evt)
        ))
    }

    createEvent(event) {
        let evtObj = {
            di: event.dh_inicio,
            df: event.dh_fim,
            titulo: event.titulo,
            channel: event.id_canal
        }
        var selector = ''
        let styles = ''
        let duration = moment.parseZone(evtObj.df).diff(moment.parseZone(evtObj.di), 'minutes')

        // check if is on the specified time range
        if ( parseInt(moment.parseZone(evtObj.di).format('HH')) < 17 ) {            
            selector = `
                        [data-time="17"] > 
                        [data-channel="${evtObj.channel}"]
                        `
            styles = `
                    height: ${duration - moment.parseZone(evtObj.di).format('mm')}px; 
                    top: 0
                    `            
        } else {

            selector = `
                        [data-time*="${moment.parseZone(evtObj.di).format('HH')}"] > 
                        [data-channel="${evtObj.channel}"]
                        `
            styles = `
                    height: ${duration}px; 
                    top: ${moment.parseZone(evtObj.di).format('mm')}px
                    `                        
        }

        

        let $el = $(selector);

        $el.append(`
            <div class="cg-event__container">
                <div class="cg-event" 
                    style="${styles}">
                    <h4>${evtObj.titulo}</h4>
                    <p>
                        ${moment.parseZone(evtObj.di).format('HH:mm')} 
                            - 
                        ${moment.parseZone(evtObj.df).format('HH:mm')}
                    </p>
                </div>
            </div>`
        )
    }

    componentDidMount() {
        const { data } = this.props
        this.renderEvents(data.channels)
    }

    render() {
        const { data } = this.props

        return (
            <div className="container-fluid">
                <ChannelGridHeader columns={data.columns} />

                <ChannelGridRow columns={data.columns} time="17" />
                <ChannelGridRow columns={data.columns} time="18" />
                <ChannelGridRow columns={data.columns} time="19" />
                <ChannelGridRow columns={data.columns} time="20" />
                <ChannelGridRow columns={data.columns} time="21" />
                <ChannelGridRow columns={data.columns} time="22" />
                <ChannelGridRow columns={data.columns} time="23" />
                <ChannelGridRow columns={data.columns} time="00" />


            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.channels
    }
}

export default connect(
    mapStateToProps,
    null
)(ChannelGrid);