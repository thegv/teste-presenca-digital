import React from 'react';
import ProductItem from '../components/ProductItem'
import { connect } from 'react-redux'

const getVisibleProducts = (products, filter) => {
    switch (filter) {
        case 'SHOW_ALL':
            return products
        case 'SHOW_VISIBLE': 
            return products.filter(p => p.exibir === 1)
        default:
            throw new Error('Unknown filter: ' + filter)
    }

}

const ProductList = ( {products} ) => {    
    return (
        <div className="columns is-multiline">
            {products.map(product =>
                <ProductItem 
                    key={product.id} 
                    product={product}
                    />
            )}
        </div>
    );
};

const mapStateToProps = ( state ) => {
    return {
        products: getVisibleProducts(state.products, 'SHOW_VISIBLE')
    }
}

export default connect(
    mapStateToProps,
    null
)(ProductList);