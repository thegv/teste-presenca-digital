import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

import { getProducts, getChannels } from './actions/'
import reducers from './reducers'
import App from './containers/App'

const middleware = process.env.NODE_ENV === 'production' ?
  [ thunk ] :
  [ thunk ]


var store = createStore(
    reducers,
    applyMiddleware(...middleware)
);

// Initial Load of data
store.dispatch(getProducts());
store.dispatch(getChannels());

render (
    <Provider store={store} >
        <App component="channel-grid" />
    </Provider>,

    document.getElementById('app')
);