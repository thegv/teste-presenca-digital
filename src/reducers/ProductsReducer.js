import { GET_PRODUCTS } from '../constants/index'

function products(state = [], action) {
    switch (action.type) {
        case GET_PRODUCTS:            
            return [
                ...state,
                ...action.products
            ]
        default:
            return state
    }
}

export default products