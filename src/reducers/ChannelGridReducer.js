
import { GET_CHANNELS_GRID } from '../constants/index'


function getColumns(channels) {
        let f = channels.map(channel => {
            return channel.id_canal
        })

        var columns = f.filter((ch, pos) => {
            return f.indexOf(ch) === pos
        })

        return columns;
    }

function channels(state = [], action) {

    switch (action.type) {
        case GET_CHANNELS_GRID:
            return Object.assign({}, state, {
                channels: [...action.channels],
                columns: [...getColumns(action.channels)]
            })
            
        default:
            return state
    }
}

export default channels