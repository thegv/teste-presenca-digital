import { combineReducers } from 'redux'
import products from './ProductsReducer'
import channels from './ChannelGridReducer'

export default combineReducers({
    products,
    channels
})