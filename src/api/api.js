import _products from './produtos.json'
import _channels from './grade-canais.json'

export default {
    getProducts(cb) {
        cb(_products)
    },

    getChannels(cb) {
        cb(_channels)
    }
}