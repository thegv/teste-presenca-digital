## HITTS - desafio front end - Guilherme Ventura
Este repositório contém os arquivos relacionados ao teste de front-end da **hitts**.

O teste foi desenvolvido utilizando **React + Redux**, com **SASS** e um padrão variante do **BEM** de css.

Também foi criado com base no projeto `create-react-app`

### Instalação
Para rodar o projeto, basta fazer um `git clone` deste repositório, acessar a pasta do projeto e executar o comando `npm install`

Após a instalação dos módulos necessários com o procedimento descrito acima, execute o comando `npm start` para inicializar o servidor local de desenvolvimento.

### Considerações sobre o código ###
A Aplicação está dividida em dois grandes componentes: `ChannelGrid` e `ProductList`, apenas um componente é exibido por vez, portanto, para alterar o componente que será exibido na tela, siga o procedimento abaixo:

 - Abra o arquivo `index.js` que está na raiz da pasta `src`
 - Procure pelo método `render` que está localizado no final do documento
 - Altere a seguinte linha : `<App component="opcao" />` para uma das seguintes opções:
 - **product-list** - Para exibir a listagem de produtos
 - **channel-grid** - Para exibir a grade de canais